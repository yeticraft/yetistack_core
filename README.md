# YetiStack

Creating your own development stack for docker with all the tools and toys.

This repository is intended for use within a class setting, and may not make much sense without the other materials.  Furthermore, it assumes that the 'development system' is a fairly virginal linux system that has not had docker installed, nor swarm mode enabled on it before beginning.

## Preparation

Along with the basics of `git`, `wget` and `curl`, thse are the basic foundation of a docker development studio.  Install each and become familiar with them before continuing on to the next section.

### Install Docker

Install docker using the instructions found [here](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-from-a-package).

You will also want to install [docker-compose](https://docs.docker.com/compose/install/).

Finally, to make your docker-ized life so much esier, install the [command-line completion](https://docs.docker.com/compose/completion/) component.

### I Am Become Swarm

Initialize the swarm master, if you have not already done so.  Simply execute the following command on your local development system.

`docker swarm init`

You can add worker nodes as desired, either now - or later. To do so, first get the token and command example by executing the following command on the master-node:

`docker swarm join-token worker`
    
Then execute the command, offered from the result, on the nodes that you wish to join into the swarm.  At any time, you can check the status of your nodes via:

`docker node ls`

## Add Powertools

All of the below can be easily installed on a system that is prepared via the above documentation, by simply executing the following command on the master node:

`curl -fsS https://bitbucket.org/yeticraft/yetistack_core/raw/master/deploy | bash`

### Registry

Now that we have a docker swarm that can maintain some persistance of data to talk to - lets create a place to store all the iamges that we are going to make and deploy to it. Make sure to add the label to the master node before executing the deploy command for the first time.

### Caching Packages (optional)

Since we will probably be re-building and re-installing applications many, many times - it might be a good idea to create a local (faster access) caching proxy for as much of the repetitious content as we can.

### Version Control

Either use a public service such as GitHub or BitBucket, or something with a little more direct controll locally such as GitLab.

### A Docker GUI

While we spend much of our time in the command-line environment, it is also nice to have the option of a web-based GUI that can simplify and visualize some of the things we are doing.  Enter Portainer!

## Optional extras

Who can have _too many_ toys, eh?

### Storage

For nodes in a swarm to be able to consistantly store data for all the containers that need persistance, we need something like a networked storage like that proided by NFS.  For more on how to install NFS, please see the appropriate documentation for your OS.  If you are using Ubuntu, look [here](https://help.ubuntu.com/lts/serverguide/network-file-system.html.en)

